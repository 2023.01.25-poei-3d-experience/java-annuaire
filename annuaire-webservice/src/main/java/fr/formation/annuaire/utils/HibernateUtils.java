package fr.formation.annuaire.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import lombok.Getter;

public class HibernateUtils {

	@Getter
	private static SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

}

package fr.formation.annuaire.models;

import java.util.Collection;
import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Person {

	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	private int age;
	
	@ToString.Exclude
	@Builder.Default
	@OneToMany(mappedBy = "person")
	@JsonIgnore
	private Collection<Address> addresses = new HashSet<>();
	
}

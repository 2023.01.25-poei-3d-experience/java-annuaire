package fr.formation.annuaire.controllers;


import java.util.Collection;
import java.util.NoSuchElementException;

import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.PersonRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Path("people")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonController {
	
	private PersonRepository personRepository = new PersonRepository();

	@GET
	public Collection<Person> findAll() {
		return personRepository.findAll();
	}
	
	@GET
	@Path("{id}")
	public Response findById(@PathParam("id") long id) {
		try {
			return Response
					.ok(personRepository.findById(id).get())
					.build();
		} catch (NoSuchElementException e) {
			return Response
					.status(Status.NOT_FOUND)
					.build();
		}
	}
	
	@POST
	public Person save(Person p) {
		return personRepository.save(p);
	}
	
	@PUT
	@Path("{id}")
	public void update(@PathParam("id") long id, Person p) {
		personRepository.update(p);
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteById(@PathParam("id") long id) {
		try {
			Person p = personRepository.findById(id).get();
			personRepository.delete(p);
			return Response.ok().build();
		} catch (NoSuchElementException e) {
			return Response
					.status(Status.NOT_FOUND)
					.build();
		}
	}
	
}

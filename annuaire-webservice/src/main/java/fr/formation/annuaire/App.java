package fr.formation.annuaire;

import org.glassfish.jersey.server.ResourceConfig;

import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("")
public class App extends ResourceConfig {

	public App() {
		packages("fr.formation.annuaire.controllers");
	}
	
}

package fr.formation.annuaire.services;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.PersonRepository;

public class PersonService {
	private PersonRepository personRepository = new PersonRepository();
	
	public Collection<Person> findAll() {
		return personRepository.findAll();
	}
	
	public Optional<Person> findById(long id) {
		return personRepository.findById(id);
	}
	
	public Person save(Person p) {
		return personRepository.save(p);
	}
	
//	public void transfer(long originId, long destinationId, double amount) {
//		Account originAccoutn = accountRepository.findById(originId);
//		Account destinationAccoutn = accountRepository.findById(destinationId);
//		originAccount.balance -= amount;
//		destinationAccoutn.balance += amount;
//		accountRepository.update(originAccount);
//		accountRepository.update(destinationAccoutn);
//	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h1>People</h1>

	${ people.size() }

	<ul>
		<c:forEach items="${people}" var="p">
		<!-- equivalent à : for (Person p : people) { ... } -->
			<li>${ p.name }</li>
		</c:forEach>
	</ul>
	
	<a href="people/add">Add</a>

</body>
</html>
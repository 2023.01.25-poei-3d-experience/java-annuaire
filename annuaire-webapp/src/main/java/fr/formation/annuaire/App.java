package fr.formation.annuaire;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import fr.formation.annuaire.models.Address;
import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.PersonRepository;
import fr.formation.annuaire.utils.HibernateUtils;

public class App {

	public static void main(String[] args) {
		PersonRepository pr = new PersonRepository();
		
		pr.findAll().forEach(System.out::println);
	
	}

	public static void test() {
		PersonRepository pr = new PersonRepository();
		
	}
	
}

package fr.formation.annuaire.repositories;

import fr.formation.annuaire.models.Person;

public class PersonRepository extends GenericRepository<Person> {

	public PersonRepository() {
		super(Person.class);
	}
	
}

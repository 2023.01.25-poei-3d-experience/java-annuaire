package fr.formation.annuaire.repositories;

import java.util.Collection;
import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.formation.annuaire.models.Address;
import fr.formation.annuaire.utils.HibernateUtils;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GenericRepository<T> {
	
	private Class<T> clazz;

	public Collection<T> findAll() {
		try (Session s = HibernateUtils.getSessionFactory().openSession()) {
			Collection<T> results = s.createQuery("from " + clazz.getSimpleName(), clazz).getResultList();
			return results;
		}
	}
	
	public Optional<T> findById(long id) {
		try (Session s = HibernateUtils.getSessionFactory().openSession()) {
			return Optional.ofNullable(s.find(clazz, id));
		}
	}
	
	public T save(T p) {
		Transaction t = null;
		try (Session s = HibernateUtils.getSessionFactory().openSession()) {
			t = s.beginTransaction();
			s.persist(p);
			t.commit();
			return p;
		} catch(Exception e) {
			if (t != null)
				t.rollback();
			throw e;
		}
	}
	
	public T update(T p) {
		Transaction t = null;
		try (Session s = HibernateUtils.getSessionFactory().openSession()) {
			t = s.beginTransaction();
			p = s.merge(p);
			t.commit();
			return p;
		} catch(Exception e) {
			if (t != null)
				t.rollback();
			throw e;
		}
	}
	
	public void delete(T p) {
		Transaction t = null;
		try (Session s = HibernateUtils.getSessionFactory().openSession()) {
			t = s.beginTransaction();
			s.remove(t);
		} catch(Exception e) {
			if (t != null)
				t.rollback();
			throw e;
		}
	}
	
}

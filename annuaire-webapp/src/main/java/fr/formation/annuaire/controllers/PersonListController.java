package fr.formation.annuaire.controllers;

import java.io.IOException;
import java.util.Collection;

import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.PersonRepository;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/")
public class PersonListController extends HttpServlet {
	
	private PersonRepository personRepository = new PersonRepository();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Collection<Person> people = personRepository.findAll();
		req.setAttribute("people", people);
		req.getRequestDispatcher("/WEB-INF/jsp/PersonList.jsp").forward(req, resp);
	}

}

package fr.formation.annuaire.controllers;

import java.io.IOException;

import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.PersonRepository;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/people/add")
public class PersonAddController extends HttpServlet {
	
	private PersonRepository personRepository = new PersonRepository();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// return the form 
		req.getRequestDispatcher("/WEB-INF/jsp/PersonAdd.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// build a Person object from the request parameters
		Person p = Person.builder()
			.name(req.getParameter("name"))
			.age(Integer.parseInt(req.getParameter("age")))
			.build();
		// save it to the database
		personRepository.save(p);
		// redirect user to the home page
		resp.sendRedirect(req.getContextPath());
	}

	
	
}
